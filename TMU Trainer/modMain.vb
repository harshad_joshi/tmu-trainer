
Friend Module Main
#Region "Combo Item Object Class"
    Structure ComboItem
        Private m_Id As Long
        Private m_Name As String
        Private m_Tag As Object
        Public Property Id() As Long
            Get
                Return m_Id
            End Get
            Set(ByVal value As Long)
                m_Id = value
            End Set
        End Property
        Public Property Name() As String
            Get
                If Me.m_Name Is Nothing OrElse Me.m_Name Is DBNull.Value Then
                    Return String.Empty
                Else
                    Return Me.m_Name
                End If
            End Get
            Set(ByVal value As String)
                If value Is Nothing OrElse value Is DBNull.Value Then
                    m_Name = String.Empty
                Else
                    m_Name = value
                End If
            End Set
        End Property

        Public Property Tag() As Object
            Get
                Return m_Tag
            End Get
            Set(ByVal value As Object)
                m_Tag = value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Return Me.Name
        End Function

    End Structure
#End Region


#Region "Fill Combos"
    Public Sub FillAwakeCombo()
        Dim objCboItem As ComboItem

        frmMain.cboHouse.Items.Clear()

        objCboItem = New ComboItem
        objCboItem.Id = 0
        objCboItem.Name = "Carboard Box"
        objCboItem.Tag = CLng(100)

        frmMain.cboHouse.Items.Add(objCboItem)


        objCboItem = New ComboItem
        objCboItem.Id = 1
        objCboItem.Name = "Tent"
        objCboItem.Tag = CLng(400)

        frmMain.cboHouse.Items.Add(objCboItem)


        objCboItem = New ComboItem
        objCboItem.Id = 2
        objCboItem.Name = "Shack"
        objCboItem.Tag = CLng(550)

        frmMain.cboHouse.Items.Add(objCboItem)


        objCboItem = New ComboItem
        objCboItem.Id = 3
        objCboItem.Name = "Small Trailer"
        objCboItem.Tag = CLng(800)

        frmMain.cboHouse.Items.Add(objCboItem)


        objCboItem = New ComboItem
        objCboItem.Id = 4
        objCboItem.Name = "Large Trailer"
        objCboItem.Tag = CLng(1100)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 5
        objCboItem.Name = "Apartment"
        objCboItem.Tag = CLng(1450)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 6
        objCboItem.Name = "Log Cabin"
        objCboItem.Tag = CLng(1850)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 7
        objCboItem.Name = "Cottage"
        objCboItem.Tag = CLng(2000)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 8
        objCboItem.Name = "Small House"
        objCboItem.Tag = CLng(2300)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 9
        objCboItem.Name = "Condo"
        objCboItem.Tag = CLng(2850)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 10
        objCboItem.Name = "Large House"
        objCboItem.Tag = CLng(3250)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 11
        objCboItem.Name = "Rural House"
        objCboItem.Tag = CLng(3700)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 12
        objCboItem.Name = "Small Mansion"
        objCboItem.Tag = CLng(4300)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 13
        objCboItem.Name = "Large Mansion"
        objCboItem.Tag = CLng(5100)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 14
        objCboItem.Name = "The Manor"
        objCboItem.Tag = CLng(6000)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 15
        objCboItem.Name = "Private Estate"
        objCboItem.Tag = CLng(7100)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 16
        objCboItem.Name = "Cattle Ranch"
        objCboItem.Tag = CLng(8300)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 17
        objCboItem.Name = "Large Private Estate"
        objCboItem.Tag = CLng(9500)

        frmMain.cboHouse.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 18
        objCboItem.Name = "The Castle"
        objCboItem.Tag = CLng(11000)

        frmMain.cboHouse.Items.Add(objCboItem)

        frmMain.cboHouse.SelectedIndex = 0

    End Sub


    Public Sub FillGTCCombo()
        Dim objCboItem As ComboItem

        frmMain.cboGTC.Items.Clear()

        objCboItem = New ComboItem
        objCboItem.Id = 0
        objCboItem.Name = "None"
        objCboItem.Tag = CLng(0)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 1
        objCboItem.Name = "Genovese Training Academy"
        objCboItem.Tag = CLng(5)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 2
        objCboItem.Name = "Gambino Training Academy"
        objCboItem.Tag = CLng(10)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 3
        objCboItem.Name = "Luciano Training Academy"
        objCboItem.Tag = CLng(15)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 4
        objCboItem.Name = "Costello Training Academy"
        objCboItem.Tag = CLng(20)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 5
        objCboItem.Name = "Capone Training Academy"
        objCboItem.Tag = CLng(25)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 6
        objCboItem.Name = "Messina Training Academy"
        objCboItem.Tag = CLng(30)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 7
        objCboItem.Name = "Adonis Training Academy"
        objCboItem.Tag = CLng(35)

        frmMain.cboGTC.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 8
        objCboItem.Name = "Gotti Training Academy"
        objCboItem.Tag = CLng(40)

        frmMain.cboGTC.Items.Add(objCboItem)

        frmMain.cboGTC.SelectedIndex = 0

    End Sub

    Public Sub FillPetKennelCombo()
        Dim objCboItem As ComboItem

        frmMain.cboKennel.Items.Clear()

        objCboItem = New ComboItem
        objCboItem.Id = 0
        objCboItem.Name = "Cardboard Box"
        objCboItem.Tag = CLng(100)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 1
        objCboItem.Name = "1 Star Kennel"
        objCboItem.Tag = CLng(350)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 2
        objCboItem.Name = "2 Star Kennel"
        objCboItem.Tag = CLng(550)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 3
        objCboItem.Name = "3 Star Kennel"
        objCboItem.Tag = CLng(800)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 4
        objCboItem.Name = "4 Star Kennel"
        objCboItem.Tag = CLng(1100)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 5
        objCboItem.Name = "5 Star Kennel"
        objCboItem.Tag = CLng(1450)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 6
        objCboItem.Name = "6 Star Kennel"
        objCboItem.Tag = CLng(1850)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 7
        objCboItem.Name = "7 Star Kennel"
        objCboItem.Tag = CLng(2000)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 8
        objCboItem.Name = "8 Star Kennel"
        objCboItem.Tag = CLng(2300)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 9
        objCboItem.Name = "9 Star Kennel"
        objCboItem.Tag = CLng(2850)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 10
        objCboItem.Name = "10 Star Kennel"
        objCboItem.Tag = CLng(3250)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 11
        objCboItem.Name = "11 Star Kennel"
        objCboItem.Tag = CLng(3650)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 12
        objCboItem.Name = "12 Star Kennel"
        objCboItem.Tag = CLng(4100)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 13
        objCboItem.Name = "13 Star Kennel"
        objCboItem.Tag = CLng(4600)

        frmMain.cboKennel.Items.Add(objCboItem)

        objCboItem = New ComboItem
        objCboItem.Id = 14
        objCboItem.Name = "14 Star Kennel"
        objCboItem.Tag = CLng(5150)

        frmMain.cboKennel.Items.Add(objCboItem)

        frmMain.cboKennel.SelectedIndex = 0
    End Sub

#End Region

#Region "Main Application Logic to get Stats & Other Values"

    Public Function GetEnergy(ByVal intLevel As Int32) As Long
        Return intLevel + 9
    End Function


    Public Function GetHitPoints(ByVal intLevel As Int32) As Long
        Return (50 + (intLevel * 50))
    End Function

    Public Function GetNerve(ByVal intLevel As Int32) As Long
        Return intLevel + 4
    End Function

    Public Function GetStatsGain() As Long
        On Error Resume Next
        Dim lngEnergy, lngAwake As Long
        Dim intIQ As Int16 = 1
        Dim intSportsBonus As Int16 = 0
        Dim intGTCBonus As Int16 = 0
        Dim booRespectBonus As Boolean

        lngEnergy = GetEnergy(frmMain.txtLevel.Text.Trim)
        lngAwake = CLng(DirectCast(frmMain.cboHouse.SelectedItem, ComboItem).Tag)
        intIQ = CInt(frmMain.txtIQ.Text.Trim) + 1
        intGTCBonus = CLng(DirectCast(frmMain.cboGTC.SelectedItem, ComboItem).Tag)

        'booRespectBonus = frmMain.chkRespectContest.Checked


        If frmMain.chkSportsStudies.Checked Then
            intSportsBonus = 5
        End If

        'Dim Result As Long 'To Directly Round Off.

        Dim decFirstPart As Decimal
        Dim decSecondPart As Decimal
        Dim decResult As Decimal

        Dim decEnAwakeIQ As Decimal

        decEnAwakeIQ = lngEnergy * lngAwake * intIQ / 1000
        'decEnAwakeIQ = decEnAwakeIQ.ToString("N0")


        decFirstPart = 1 + (intGTCBonus / 100)

        'decSecondPart = decEnAwakeIQ + CDec((intSportsBonus / 100) * decEnAwakeIQ).ToString("N0")
        decSecondPart = decEnAwakeIQ + (intSportsBonus / 100) * decEnAwakeIQ


        'decResult = CDec(decFirstPart * decSecondPart).ToString("N0")
        decResult = decFirstPart * decSecondPart

        If booRespectBonus Then
            Dim level As Int16 = frmMain.txtLevel.Text.Trim

            Select Case level
                Case 151 To 200
                    decResult *= 1.3
                Case Is > 200
                    decResult *= 1.1
                Case Else
                    decResult *= 1
            End Select

        End If

        Return decResult

    End Function

    Public Function GetPetStatsGain() As Long
        On Error Resume Next
        Dim lngEnergy, lngAwake As Long
        Dim intPetHandlingBonus As Int16 = 0

        lngEnergy = GetEnergy(Val(frmMain.txtPetLevel.Text.Trim))
        lngAwake = CLng(DirectCast(frmMain.cboKennel.SelectedItem, ComboItem).Tag)

        If frmMain.chkPetSchool.Checked Then
            intPetHandlingBonus = 5
        End If

        Dim decResult, decResult2 As Decimal

        decResult = lngEnergy * lngAwake / 100

        decResult2 = decResult + (intPetHandlingBonus / 100) * decResult


        Return decResult2


    End Function

    Public Function PowerTrain(Optional ByVal Points As Long = 0) As Long
        On Error Resume Next
        Dim lngPoints As Long

        If Points <> 0 Then
            lngPoints = Points
        Else
            lngPoints = CLng(Val(frmMain.txtPoints.Text.Trim))
        End If



        Dim lngAwake As Long
        Dim lngEnergy As Long

        lngEnergy = GetEnergy(frmMain.txtLevel.Text.Trim)
        lngAwake = CLng(DirectCast(frmMain.cboHouse.SelectedItem, ComboItem).Tag)

        Dim lngStatGain As Long

        lngStatGain = GetStatsGain()

        Dim Result1, Result2, Result3, Result4, Result As Decimal



        Result1 = (lngAwake - (lngEnergy * 2)) / lngAwake




        Dim tmpResult As Decimal

        tmpResult = RoundDown(Result1, 2)


        If tmpResult < Result1 Then
            Result1 = tmpResult + 0.01
        End If


        Result2 = 100 - (100 * Result1)

        Result3 = lngPoints / (Result2 + 10)

        Result4 = Math.Floor(Result3)

        Result = Result4 * GetStatsGain()

        Return Result


    End Function


    Function RoundDown(ByVal dVal As Decimal, ByVal Decimals As Integer) As Decimal
        Dim Factor As Double, fVal As Double
        Dim iVal As Integer
        Factor = Math.Exp(Math.Log(10) * Decimals)
        fVal = dVal * Factor
        iVal = Int(fVal)
        RoundDown = iVal / Factor
    End Function


    Public Function PetPowerTrain(Optional ByVal Points As Long = 0) As Long
        On Error Resume Next
        Dim lngPoints As Long

        If Points <> 0 Then
            lngPoints = Points
        Else
            lngPoints = CLng(Val(frmMain.txtPetPoints.Text.Trim))
        End If



        Dim lngAwake As Long
        Dim lngEnergy As Long

        lngEnergy = GetEnergy(frmMain.txtPetLevel.Text.Trim)
        lngAwake = CLng(DirectCast(frmMain.cboKennel.SelectedItem, ComboItem).Tag)

        Dim lngStatGain As Long

        lngStatGain = GetStatsGain()

        Dim Result1, Result2, Result3, Result4, Result As Decimal



        Result1 = (lngAwake - (lngEnergy * 2)) / lngAwake




        Dim tmpResult As Decimal

        tmpResult = RoundDown(Result1, 2)


        If tmpResult < Result1 Then
            Result1 = tmpResult + 0.01
        End If


        Result2 = 100 - (100 * Result1)

        Result3 = lngPoints / (Result2 + 10)

        Result4 = Math.Floor(Result3)

        Result = Result4 * GetPetStatsGain()

        Return Result
    End Function


#End Region

#Region "Settings File Handling"
    'The Basic Purpose of this Region is to Save & Load Settings on Startup
    ' two Functions
    '       1.) Save Settings -- Send to User Settings File
    '       2.) Read Settings -- Read Settings and Send to Form

    Public Function SaveSettings() As Boolean
        Try
            My.Settings.userLevel = CInt(frmMain.txtLevel.Text)
            My.Settings.userIQ = CByte(frmMain.txtIQ.Text)
            My.Settings.userHouse = CByte(frmMain.cboHouse.SelectedIndex)
            My.Settings.userCompound = CByte(frmMain.cboGTC.SelectedIndex)
            My.Settings.userSchoolingDone = IIf(frmMain.chkSportsStudies.Checked, True, False)

            My.Settings.petLevel = CInt(frmMain.txtPetLevel.Text)
            My.Settings.petKennel = CByte(frmMain.cboKennel.SelectedIndex)
            My.Settings.petSchoolingDone = IIf(frmMain.chkPetSchool.Checked, True, False)

            My.Settings.appTabOpen = CByte(frmMain.SaveTabPage)

            My.Settings.Save()
            Return True
        Catch ex As Exception
            MsgBox("There was an Error Saving the Settings--" & vbCrLf & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation + MsgBoxStyle.SystemModal, "TMU Trainer")
            Return False
        End Try
    End Function

    Public Function ReadSettings() As Boolean
        Try
            frmMain.txtLevel.Text = My.Settings.userLevel.ToString
            frmMain.txtIQ.Text = My.Settings.userIQ.ToString
            frmMain.cboHouse.SelectedIndex = CInt(My.Settings.userHouse.ToString)
            frmMain.cboGTC.SelectedIndex = CInt(My.Settings.userCompound.ToString)
            frmMain.chkSportsStudies.Checked = CBool(My.Settings.userSchoolingDone)

            frmMain.txtPetLevel.Text = My.Settings.petLevel.ToString
            frmMain.cboKennel.SelectedIndex = CInt(My.Settings.petKennel.ToString)
            frmMain.chkPetSchool.Checked = CBool(My.Settings.petSchoolingDone)

            frmMain.TabOpen = CInt(My.Settings.appTabOpen)
            Return True
        Catch ex As Exception
            MsgBox("There was an Error Reading the Settings--" & vbCrLf & ex.Message, MsgBoxStyle.OkOnly + MsgBoxStyle.Exclamation + MsgBoxStyle.SystemModal, "TMU Trainer")
            Return False
        End Try
    End Function

#End Region

#Region "Proposed v2.0 Section Find How many points are required to get to particular stats"
    '1.) Find out Total Points Required for one Refill / Train
    'Compare till StatGain = PowerGain by increasing points by 1

    '2.) Divide Required Stats by StatGain to get Total Number of Trains to get target stats.

    Public Function TargetTrain(ByVal Stats2Gain As Long, Optional ByVal booPetTrain As Boolean = False) As Long
        Dim statsPerTrain As Long
        Dim PointsPerTrain As Byte = 10
        Dim TotalPointsRequired As Long

        If Not booPetTrain Then
            While GetStatsGain() <> PowerTrain(PointsPerTrain)
                PointsPerTrain += 1
            End While
            statsPerTrain = GetStatsGain()
        Else
            While GetPetStatsGain() <> PetPowerTrain(PointsPerTrain)
                PointsPerTrain += 1
            End While
            statsPerTrain = GetPetStatsGain()
        End If



        Dim Remainder As Long

        If Stats2Gain < statsPerTrain Then Exit Function

        Remainder = Stats2Gain Mod statsPerTrain

        Stats2Gain += Remainder

        TotalPointsRequired = PointsPerTrain * (Stats2Gain / statsPerTrain)

        Return TotalPointsRequired
    End Function

#End Region
End Module

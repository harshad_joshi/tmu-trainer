
Public Class frmMain

    Public Enum OpenTab
        MobsterPage = 0
        PetPage = 1
    End Enum
    Public Property TabOpen As OpenTab = OpenTab.MobsterPage


    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        SaveSettings()
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '// First populate all the Combos
        FillAwakeCombo()
        FillGTCCombo()
        FillPetKennelCombo()


        txtPoints.Text = 0
        txtPetPoints.Text = 0

        '// Try and load the settings if successful exit, else Load defaults.
        If ReadSettings() Then tbMain.SelectedTab = ReturnTabPage() : Exit Sub


        txtLevel.Text = 1
        txtIQ.Text = 1
        txtPetLevel.Text = 1


        ''Read Settings

    End Sub

    Private Function ReturnTabPage() As TabPage
        If TabOpen = OpenTab.MobsterPage Then
            Return TabPage1
        Else
            Return TabPage2
        End If
    End Function

    Public Function SaveTabPage() As OpenTab
        If tbMain.SelectedTab Is TabPage1 Then
            Return OpenTab.MobsterPage
        Else
            Return OpenTab.PetPage
        End If
    End Function

    Private Sub cboHouse_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboHouse.SelectedIndexChanged
        lblValAwake.Text = CStr("Awake: " & DirectCast(cboHouse.SelectedItem, ComboItem).Tag)
        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub

    Private Sub cboGTC_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboGTC.SelectedIndexChanged
        lblGTCBonus.Text = CStr("Bonus: " & DirectCast(cboGTC.SelectedItem, ComboItem).Tag & "%")
        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub

    Private Sub DefKeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLevel.KeyPress, txtIQ.KeyPress, txtPoints.KeyPress, txtPetLevel.KeyPress, txtPetPoints.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = True
        End If
        If Char.IsControl(e.KeyChar) Then
            If e.KeyChar <> CChar(ChrW(System.ConsoleKey.Backspace)) Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtLevel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLevel.TextChanged
        Dim intLevel As Long

        If txtLevel.Text.Trim.Length = 0 OrElse txtLevel.Text.Trim Is String.Empty Then
            txtLevel.Text = 1
        End If


        intLevel = Val(txtLevel.Text.Trim)
        If intLevel < 1 Then
            txtLevel.Text = 1
        End If

        If intLevel > 600 Then
            txtLevel.Text = 600
        End If

        'If Val(txtLevel.Text.Trim) < 1 Or Val(txtLevel.Text.Trim) > 500 Then
        '    txtLevel.Text = 1
        'End If


        txtHitPoints.Text = Format(GetHitPoints(txtLevel.Text.Trim), "N0")
        txtNerve.Text = Format(GetNerve(txtLevel.Text.Trim), "N0")
        txtEnergy.Text = Format(GetEnergy(txtLevel.Text.Trim), "N0")

        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub

    Private Sub txtPoints_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPoints.TextChanged
        If Val(txtPoints.Text.Trim) < 0 Then
            txtPoints.Text = 0
        End If
        If txtPoints.Text.Trim.Length = 0 OrElse txtPoints.Text.Trim Is String.Empty Then
            txtPoints.Text = 0
        End If

        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub

    Private Sub txtIQ_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtIQ.TextChanged
        Dim intIQ As Int16

        If txtIQ.Text.Trim.Length = 0 OrElse txtIQ.Text.Trim Is String.Empty Then
            txtIQ.Text = 1
        End If

        intIQ = Val(txtIQ.Text.Trim)

        If intIQ < 1 Then
            txtIQ.Text = 1
        End If

        If intIQ > 36 Then
            txtIQ.Text = 36
        End If

        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub

    Private Sub chkSportsStudies_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSportsStudies.CheckedChanged
        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub

    Private Sub chkRespectContest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        txtSingleTrain.Text = Format(GetStatsGain(), "N0")
        txtPointTrain.Text = Format(PowerTrain(), "N0")
    End Sub


    Private Sub cboKennel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboKennel.SelectedIndexChanged
        lblKennelAwake.Text = CStr("Awake: " & DirectCast(cboKennel.SelectedItem, ComboItem).Tag)
        txtPetStatGain.Text = Format(GetPetStatsGain(), "N0")
    End Sub

    Private Sub txtPetLevel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPetLevel.TextChanged
        Dim intLevel As Long

        If txtPetLevel.Text.Trim.Length = 0 OrElse txtPetLevel.Text.Trim Is String.Empty Then
            txtPetLevel.Text = 1
        End If


        intLevel = Val(txtPetLevel.Text.Trim)
        If intLevel < 1 Then
            txtPetLevel.Text = 1
        End If

        If intLevel > 600 Then
            txtPetLevel.Text = 600
        End If

        txtPetHP.Text = Format(GetHitPoints(txtPetLevel.Text.Trim), "N0")
        txtPetNerve.Text = Format(GetNerve(txtPetLevel.Text.Trim), "N0")
        txtPetEnergy.Text = Format(GetEnergy(txtPetLevel.Text.Trim), "N0")

        txtPetStatGain.Text = Format(GetPetStatsGain(), "N0")
        txtPetPointTrain.Text = Format(PetPowerTrain(), "N0")
        'txtPointTrain.Text = Format(PowerTrain(), "N0")

    End Sub



    Private Sub chkPetSchool_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPetSchool.CheckedChanged
        txtPetStatGain.Text = Format(GetPetStatsGain(), "N0")
        txtPetPointTrain.Text = Format(PetPowerTrain(), "N0")
    End Sub

    Private Sub txtPetPoints_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPetPoints.TextChanged
        If Val(txtPetPoints.Text.Trim) < 0 Then
            txtPoints.Text = 0
        End If
        If txtPetPoints.Text.Trim.Length = 0 OrElse txtPoints.Text.Trim Is String.Empty Then
            txtPoints.Text = 0
        End If

        txtPetStatGain.Text = Format(GetPetStatsGain(), "N0")
        txtPetPointTrain.Text = Format(PetPowerTrain(), "N0")
    End Sub

    Private Sub btnTargetTrain_Click(sender As System.Object, e As System.EventArgs) Handles btnTargetTrain.Click
        If MsgBox("The Target Trainer Requires Values filled in this form, please ensure you have filled your `Level`, Selected the right `House`, `Gang-Compound`, `IQ`, `Sports Studies` done before continuing?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Are you Sure you wish to launch the Target Trainer?") = MsgBoxResult.Yes Then
            Dim frm As New frmTargetTrain
            frm.ShowDialog(Me)
        End If

    End Sub

    Private Sub btnPetTargetTrain_Click(sender As System.Object, e As System.EventArgs) Handles btnPetTargetTrain.Click
        If MsgBox("The Target Trainer Requires Values filled in this form, please ensure you have filled your `Pet Level`, Selected the right `Kennel`, `Pet Handling in School` done before continuing?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Are you Sure you wish to launch the Pet Target Trainer?") = MsgBoxResult.Yes Then
            Dim frm As New frmPetTargetTrain
            frm.ShowDialog(Me)
        End If
    End Sub
End Class



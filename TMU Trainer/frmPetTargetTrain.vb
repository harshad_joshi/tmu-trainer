﻿Public Class frmPetTargetTrain

    Private Sub frmPetTargetTrain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.txtLevel.Text = frmMain.txtPetLevel.Text.Trim
        Me.txtAwake.Text = Format(DirectCast(frmMain.cboKennel.SelectedItem, ComboItem).Tag, "N0")
        Me.txtPetStudies.Text = IIf(frmMain.chkPetSchool.Checked, "Completed", "Not Complete")
        Me.txtStatGain.Text = Format(GetPetStatsGain(), "N0")
    End Sub

    Private Sub DefKeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTargetStats.KeyPress
        If Char.IsLetter(e.KeyChar) Then
            e.Handled = True
        End If
        If Char.IsControl(e.KeyChar) Then
            If e.KeyChar <> CChar(ChrW(System.ConsoleKey.Backspace)) Then
                e.Handled = True
            End If
        End If
        If Char.IsSymbol(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnTargetPoints_Click(sender As System.Object, e As System.EventArgs) Handles btnTargetPoints.Click
        txtTargetStats.Text = txtTargetStats.Text.Replace(",", "")
        If Val(txtTargetStats.Text.Trim) < 0 Then MsgBox("Please Enter a Valid Number of Target Stats", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "TMU Trainer-") : Exit Sub
        If Val(txtTargetStats.Text.Trim) < GetStatsGain() Then MsgBox("Target Stats cannot be Less than What you get in One Train", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, "TMU Trainer-") : Exit Sub

        txtPointsRequired.Text = Format(TargetTrain(Val(txtTargetStats.Text.Trim), True), "N0")
    End Sub

End Class
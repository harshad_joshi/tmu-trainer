﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPetTargetTrain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPetTargetTrain))
        Me.grpDetails = New System.Windows.Forms.GroupBox()
        Me.txtStatGain = New System.Windows.Forms.TextBox()
        Me.lblStatGain = New System.Windows.Forms.Label()
        Me.txtPetStudies = New System.Windows.Forms.TextBox()
        Me.lblPetStudies = New System.Windows.Forms.Label()
        Me.txtAwake = New System.Windows.Forms.TextBox()
        Me.lblAwake = New System.Windows.Forms.Label()
        Me.txtLevel = New System.Windows.Forms.TextBox()
        Me.lblLevel = New System.Windows.Forms.Label()
        Me.btnTargetPoints = New System.Windows.Forms.Button()
        Me.txtPointsRequired = New System.Windows.Forms.TextBox()
        Me.lblPointsRequired = New System.Windows.Forms.Label()
        Me.txtTargetStats = New System.Windows.Forms.TextBox()
        Me.lblTargetStats = New System.Windows.Forms.Label()
        Me.grpDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpDetails
        '
        Me.grpDetails.Controls.Add(Me.txtStatGain)
        Me.grpDetails.Controls.Add(Me.lblStatGain)
        Me.grpDetails.Controls.Add(Me.txtPetStudies)
        Me.grpDetails.Controls.Add(Me.lblPetStudies)
        Me.grpDetails.Controls.Add(Me.txtAwake)
        Me.grpDetails.Controls.Add(Me.lblAwake)
        Me.grpDetails.Controls.Add(Me.txtLevel)
        Me.grpDetails.Controls.Add(Me.lblLevel)
        Me.grpDetails.Location = New System.Drawing.Point(3, 2)
        Me.grpDetails.Name = "grpDetails"
        Me.grpDetails.Size = New System.Drawing.Size(249, 226)
        Me.grpDetails.TabIndex = 17
        Me.grpDetails.TabStop = False
        Me.grpDetails.Text = "Details"
        '
        'txtStatGain
        '
        Me.txtStatGain.BackColor = System.Drawing.SystemColors.Window
        Me.txtStatGain.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatGain.ForeColor = System.Drawing.Color.Maroon
        Me.txtStatGain.Location = New System.Drawing.Point(106, 178)
        Me.txtStatGain.Name = "txtStatGain"
        Me.txtStatGain.ReadOnly = True
        Me.txtStatGain.Size = New System.Drawing.Size(136, 23)
        Me.txtStatGain.TabIndex = 23
        '
        'lblStatGain
        '
        Me.lblStatGain.AutoSize = True
        Me.lblStatGain.Location = New System.Drawing.Point(16, 181)
        Me.lblStatGain.Name = "lblStatGain"
        Me.lblStatGain.Size = New System.Drawing.Size(54, 13)
        Me.lblStatGain.TabIndex = 22
        Me.lblStatGain.Text = "Stat Gain:"
        '
        'txtPetStudies
        '
        Me.txtPetStudies.BackColor = System.Drawing.SystemColors.Window
        Me.txtPetStudies.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPetStudies.ForeColor = System.Drawing.Color.Maroon
        Me.txtPetStudies.Location = New System.Drawing.Point(106, 129)
        Me.txtPetStudies.Name = "txtPetStudies"
        Me.txtPetStudies.ReadOnly = True
        Me.txtPetStudies.Size = New System.Drawing.Size(136, 23)
        Me.txtPetStudies.TabIndex = 21
        '
        'lblPetStudies
        '
        Me.lblPetStudies.AutoSize = True
        Me.lblPetStudies.Location = New System.Drawing.Point(16, 132)
        Me.lblPetStudies.Name = "lblPetStudies"
        Me.lblPetStudies.Size = New System.Drawing.Size(76, 13)
        Me.lblPetStudies.TabIndex = 20
        Me.lblPetStudies.Text = "Pet Schooling:"
        '
        'txtAwake
        '
        Me.txtAwake.BackColor = System.Drawing.SystemColors.Window
        Me.txtAwake.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAwake.ForeColor = System.Drawing.Color.Maroon
        Me.txtAwake.Location = New System.Drawing.Point(106, 85)
        Me.txtAwake.Name = "txtAwake"
        Me.txtAwake.ReadOnly = True
        Me.txtAwake.Size = New System.Drawing.Size(136, 23)
        Me.txtAwake.TabIndex = 15
        '
        'lblAwake
        '
        Me.lblAwake.AutoSize = True
        Me.lblAwake.Location = New System.Drawing.Point(16, 88)
        Me.lblAwake.Name = "lblAwake"
        Me.lblAwake.Size = New System.Drawing.Size(43, 13)
        Me.lblAwake.TabIndex = 14
        Me.lblAwake.Text = "Awake:"
        '
        'txtLevel
        '
        Me.txtLevel.BackColor = System.Drawing.SystemColors.Window
        Me.txtLevel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLevel.ForeColor = System.Drawing.Color.Maroon
        Me.txtLevel.Location = New System.Drawing.Point(106, 47)
        Me.txtLevel.Name = "txtLevel"
        Me.txtLevel.ReadOnly = True
        Me.txtLevel.Size = New System.Drawing.Size(136, 23)
        Me.txtLevel.TabIndex = 13
        '
        'lblLevel
        '
        Me.lblLevel.AutoSize = True
        Me.lblLevel.Location = New System.Drawing.Point(16, 50)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(36, 13)
        Me.lblLevel.TabIndex = 12
        Me.lblLevel.Text = "Level:"
        '
        'btnTargetPoints
        '
        Me.btnTargetPoints.Location = New System.Drawing.Point(65, 275)
        Me.btnTargetPoints.Name = "btnTargetPoints"
        Me.btnTargetPoints.Size = New System.Drawing.Size(140, 30)
        Me.btnTargetPoints.TabIndex = 1
        Me.btnTargetPoints.Text = "Tell me!"
        Me.btnTargetPoints.UseVisualStyleBackColor = True
        '
        'txtPointsRequired
        '
        Me.txtPointsRequired.BackColor = System.Drawing.SystemColors.Window
        Me.txtPointsRequired.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPointsRequired.ForeColor = System.Drawing.Color.SeaGreen
        Me.txtPointsRequired.Location = New System.Drawing.Point(109, 333)
        Me.txtPointsRequired.Name = "txtPointsRequired"
        Me.txtPointsRequired.ReadOnly = True
        Me.txtPointsRequired.Size = New System.Drawing.Size(136, 23)
        Me.txtPointsRequired.TabIndex = 2
        '
        'lblPointsRequired
        '
        Me.lblPointsRequired.AutoSize = True
        Me.lblPointsRequired.Location = New System.Drawing.Point(19, 336)
        Me.lblPointsRequired.Name = "lblPointsRequired"
        Me.lblPointsRequired.Size = New System.Drawing.Size(85, 13)
        Me.lblPointsRequired.TabIndex = 22
        Me.lblPointsRequired.Text = "Points Required:"
        '
        'txtTargetStats
        '
        Me.txtTargetStats.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTargetStats.Location = New System.Drawing.Point(109, 234)
        Me.txtTargetStats.Name = "txtTargetStats"
        Me.txtTargetStats.Size = New System.Drawing.Size(136, 23)
        Me.txtTargetStats.TabIndex = 0
        '
        'lblTargetStats
        '
        Me.lblTargetStats.AutoSize = True
        Me.lblTargetStats.Location = New System.Drawing.Point(19, 237)
        Me.lblTargetStats.Name = "lblTargetStats"
        Me.lblTargetStats.Size = New System.Drawing.Size(68, 13)
        Me.lblTargetStats.TabIndex = 20
        Me.lblTargetStats.Text = "Target Stats:"
        '
        'frmPetTargetTrain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(257, 382)
        Me.Controls.Add(Me.btnTargetPoints)
        Me.Controls.Add(Me.txtPointsRequired)
        Me.Controls.Add(Me.lblPointsRequired)
        Me.Controls.Add(Me.txtTargetStats)
        Me.Controls.Add(Me.lblTargetStats)
        Me.Controls.Add(Me.grpDetails)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPetTargetTrain"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pet Target Train"
        Me.grpDetails.ResumeLayout(False)
        Me.grpDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grpDetails As System.Windows.Forms.GroupBox
    Friend WithEvents txtStatGain As System.Windows.Forms.TextBox
    Friend WithEvents lblStatGain As System.Windows.Forms.Label
    Friend WithEvents txtPetStudies As System.Windows.Forms.TextBox
    Friend WithEvents lblPetStudies As System.Windows.Forms.Label
    Friend WithEvents txtAwake As System.Windows.Forms.TextBox
    Friend WithEvents lblAwake As System.Windows.Forms.Label
    Friend WithEvents txtLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents btnTargetPoints As System.Windows.Forms.Button
    Friend WithEvents txtPointsRequired As System.Windows.Forms.TextBox
    Friend WithEvents lblPointsRequired As System.Windows.Forms.Label
    Friend WithEvents txtTargetStats As System.Windows.Forms.TextBox
    Friend WithEvents lblTargetStats As System.Windows.Forms.Label
End Class

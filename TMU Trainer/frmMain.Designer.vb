<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ttpDefault = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkSportsStudies = New System.Windows.Forms.CheckBox()
        Me.txtIQ = New System.Windows.Forms.TextBox()
        Me.cboGTC = New System.Windows.Forms.ComboBox()
        Me.cboHouse = New System.Windows.Forms.ComboBox()
        Me.txtPoints = New System.Windows.Forms.TextBox()
        Me.txtLevel = New System.Windows.Forms.TextBox()
        Me.cboKennel = New System.Windows.Forms.ComboBox()
        Me.chkPetSchool = New System.Windows.Forms.CheckBox()
        Me.txtPetLevel = New System.Windows.Forms.TextBox()
        Me.txtPetPoints = New System.Windows.Forms.TextBox()
        Me.tbMain = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.grpTraining = New System.Windows.Forms.GroupBox()
        Me.txtPointTrain = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtSingleTrain = New System.Windows.Forms.TextBox()
        Me.lblStatsGained = New System.Windows.Forms.Label()
        Me.txtNerve = New System.Windows.Forms.TextBox()
        Me.lblNerve = New System.Windows.Forms.Label()
        Me.txtEnergy = New System.Windows.Forms.TextBox()
        Me.lblEnergy = New System.Windows.Forms.Label()
        Me.txtHitPoints = New System.Windows.Forms.TextBox()
        Me.lblHitpoints = New System.Windows.Forms.Label()
        Me.grpMain = New System.Windows.Forms.GroupBox()
        Me.btnTargetTrain = New System.Windows.Forms.Button()
        Me.lblIQ = New System.Windows.Forms.Label()
        Me.lblGTCBonus = New System.Windows.Forms.Label()
        Me.lblTrainingCompound = New System.Windows.Forms.Label()
        Me.lblValAwake = New System.Windows.Forms.Label()
        Me.lblHouse = New System.Windows.Forms.Label()
        Me.lblPoints = New System.Windows.Forms.Label()
        Me.lblLevel = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtPetNerve = New System.Windows.Forms.TextBox()
        Me.lblPetNerve = New System.Windows.Forms.Label()
        Me.txtPetPointTrain = New System.Windows.Forms.TextBox()
        Me.lblPointTrain = New System.Windows.Forms.Label()
        Me.txtPetStatGain = New System.Windows.Forms.TextBox()
        Me.lblStatGain = New System.Windows.Forms.Label()
        Me.txtPetEnergy = New System.Windows.Forms.TextBox()
        Me.lblPetEnergy = New System.Windows.Forms.Label()
        Me.txtPetHP = New System.Windows.Forms.TextBox()
        Me.lblPetHP = New System.Windows.Forms.Label()
        Me.grpPetDetails = New System.Windows.Forms.GroupBox()
        Me.lblPetPoints = New System.Windows.Forms.Label()
        Me.lblKennelAwake = New System.Windows.Forms.Label()
        Me.lblPetLevel = New System.Windows.Forms.Label()
        Me.lblKennel = New System.Windows.Forms.Label()
        Me.btnPetTargetTrain = New System.Windows.Forms.Button()
        Me.tbMain.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.grpTraining.SuspendLayout()
        Me.grpMain.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpPetDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'ttpDefault
        '
        Me.ttpDefault.IsBalloon = True
        Me.ttpDefault.ShowAlways = True
        Me.ttpDefault.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ttpDefault.ToolTipTitle = "TMU Trainer"
        '
        'chkSportsStudies
        '
        Me.chkSportsStudies.AutoSize = True
        Me.chkSportsStudies.Location = New System.Drawing.Point(207, 107)
        Me.chkSportsStudies.Name = "chkSportsStudies"
        Me.chkSportsStudies.Size = New System.Drawing.Size(147, 17)
        Me.chkSportsStudies.TabIndex = 5
        Me.chkSportsStudies.Text = "Completed Sports Studies"
        Me.ttpDefault.SetToolTip(Me.chkSportsStudies, "Select this option if you have Completed the Sports Studies Education")
        Me.chkSportsStudies.UseVisualStyleBackColor = True
        '
        'txtIQ
        '
        Me.txtIQ.Location = New System.Drawing.Point(68, 105)
        Me.txtIQ.Name = "txtIQ"
        Me.txtIQ.Size = New System.Drawing.Size(83, 20)
        Me.txtIQ.TabIndex = 4
        Me.ttpDefault.SetToolTip(Me.txtIQ, "Enter your Mobsters IQ here")
        '
        'cboGTC
        '
        Me.cboGTC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGTC.FormattingEnabled = True
        Me.cboGTC.Location = New System.Drawing.Point(322, 65)
        Me.cboGTC.Name = "cboGTC"
        Me.cboGTC.Size = New System.Drawing.Size(171, 21)
        Me.cboGTC.TabIndex = 3
        Me.ttpDefault.SetToolTip(Me.cboGTC, "Select the Gang Training Compound your gang has. If you are not in a gang, use th" & _
        "e default: None")
        '
        'cboHouse
        '
        Me.cboHouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboHouse.FormattingEnabled = True
        Me.cboHouse.Location = New System.Drawing.Point(322, 28)
        Me.cboHouse.Name = "cboHouse"
        Me.cboHouse.Size = New System.Drawing.Size(171, 21)
        Me.cboHouse.TabIndex = 1
        Me.ttpDefault.SetToolTip(Me.cboHouse, "Select the House your Mobster Resides in.")
        '
        'txtPoints
        '
        Me.txtPoints.Location = New System.Drawing.Point(68, 65)
        Me.txtPoints.Name = "txtPoints"
        Me.txtPoints.Size = New System.Drawing.Size(83, 20)
        Me.txtPoints.TabIndex = 2
        Me.ttpDefault.SetToolTip(Me.txtPoints, "If you are going to use Points to train, enter the amount of Points you are going" & _
        " to use over here.")
        '
        'txtLevel
        '
        Me.txtLevel.Location = New System.Drawing.Point(68, 28)
        Me.txtLevel.Name = "txtLevel"
        Me.txtLevel.Size = New System.Drawing.Size(83, 20)
        Me.txtLevel.TabIndex = 0
        Me.ttpDefault.SetToolTip(Me.txtLevel, "Enter your Mobster Level")
        '
        'cboKennel
        '
        Me.cboKennel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboKennel.FormattingEnabled = True
        Me.cboKennel.Location = New System.Drawing.Point(308, 34)
        Me.cboKennel.Name = "cboKennel"
        Me.cboKennel.Size = New System.Drawing.Size(171, 21)
        Me.cboKennel.TabIndex = 1
        Me.ttpDefault.SetToolTip(Me.cboKennel, "Select the Kennel your Pet has.")
        '
        'chkPetSchool
        '
        Me.chkPetSchool.AutoSize = True
        Me.chkPetSchool.Location = New System.Drawing.Point(254, 73)
        Me.chkPetSchool.Name = "chkPetSchool"
        Me.chkPetSchool.Size = New System.Drawing.Size(187, 17)
        Me.chkPetSchool.TabIndex = 3
        Me.chkPetSchool.Text = "Completed Pet Handling in School"
        Me.ttpDefault.SetToolTip(Me.chkPetSchool, "Select this option if you have Completed the Pet Handling Studies in School")
        Me.chkPetSchool.UseVisualStyleBackColor = True
        '
        'txtPetLevel
        '
        Me.txtPetLevel.Location = New System.Drawing.Point(68, 34)
        Me.txtPetLevel.Name = "txtPetLevel"
        Me.txtPetLevel.Size = New System.Drawing.Size(83, 20)
        Me.txtPetLevel.TabIndex = 0
        Me.ttpDefault.SetToolTip(Me.txtPetLevel, "Enter your Pet Level")
        '
        'txtPetPoints
        '
        Me.txtPetPoints.Location = New System.Drawing.Point(68, 71)
        Me.txtPetPoints.Name = "txtPetPoints"
        Me.txtPetPoints.Size = New System.Drawing.Size(83, 20)
        Me.txtPetPoints.TabIndex = 2
        Me.ttpDefault.SetToolTip(Me.txtPetPoints, "If you are going to use Points to train your pet, enter the amount of Points you " & _
        "are going to use over here.")
        '
        'tbMain
        '
        Me.tbMain.Controls.Add(Me.TabPage1)
        Me.tbMain.Controls.Add(Me.TabPage2)
        Me.tbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbMain.Location = New System.Drawing.Point(0, 0)
        Me.tbMain.Name = "tbMain"
        Me.tbMain.SelectedIndex = 0
        Me.tbMain.Size = New System.Drawing.Size(614, 344)
        Me.tbMain.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grpTraining)
        Me.TabPage1.Controls.Add(Me.grpMain)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(5)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(606, 318)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Mobster Trainer"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'grpTraining
        '
        Me.grpTraining.Controls.Add(Me.txtPointTrain)
        Me.grpTraining.Controls.Add(Me.Label2)
        Me.grpTraining.Controls.Add(Me.txtSingleTrain)
        Me.grpTraining.Controls.Add(Me.lblStatsGained)
        Me.grpTraining.Controls.Add(Me.txtNerve)
        Me.grpTraining.Controls.Add(Me.lblNerve)
        Me.grpTraining.Controls.Add(Me.txtEnergy)
        Me.grpTraining.Controls.Add(Me.lblEnergy)
        Me.grpTraining.Controls.Add(Me.txtHitPoints)
        Me.grpTraining.Controls.Add(Me.lblHitpoints)
        Me.grpTraining.Location = New System.Drawing.Point(9, 154)
        Me.grpTraining.Name = "grpTraining"
        Me.grpTraining.Size = New System.Drawing.Size(591, 158)
        Me.grpTraining.TabIndex = 3
        Me.grpTraining.TabStop = False
        Me.grpTraining.Text = "Training Predictions"
        '
        'txtPointTrain
        '
        Me.txtPointTrain.BackColor = System.Drawing.Color.White
        Me.txtPointTrain.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPointTrain.ForeColor = System.Drawing.Color.OrangeRed
        Me.txtPointTrain.Location = New System.Drawing.Point(380, 78)
        Me.txtPointTrain.Name = "txtPointTrain"
        Me.txtPointTrain.ReadOnly = True
        Me.txtPointTrain.Size = New System.Drawing.Size(112, 23)
        Me.txtPointTrain.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(316, 81)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Point Train:"
        '
        'txtSingleTrain
        '
        Me.txtSingleTrain.BackColor = System.Drawing.Color.White
        Me.txtSingleTrain.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSingleTrain.ForeColor = System.Drawing.Color.SeaGreen
        Me.txtSingleTrain.Location = New System.Drawing.Point(142, 78)
        Me.txtSingleTrain.Name = "txtSingleTrain"
        Me.txtSingleTrain.ReadOnly = True
        Me.txtSingleTrain.Size = New System.Drawing.Size(112, 23)
        Me.txtSingleTrain.TabIndex = 3
        '
        'lblStatsGained
        '
        Me.lblStatsGained.AutoSize = True
        Me.lblStatsGained.Location = New System.Drawing.Point(82, 81)
        Me.lblStatsGained.Name = "lblStatsGained"
        Me.lblStatsGained.Size = New System.Drawing.Size(54, 13)
        Me.lblStatsGained.TabIndex = 8
        Me.lblStatsGained.Text = "Stat Gain:"
        '
        'txtNerve
        '
        Me.txtNerve.BackColor = System.Drawing.Color.White
        Me.txtNerve.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNerve.ForeColor = System.Drawing.Color.Sienna
        Me.txtNerve.Location = New System.Drawing.Point(434, 30)
        Me.txtNerve.Name = "txtNerve"
        Me.txtNerve.ReadOnly = True
        Me.txtNerve.Size = New System.Drawing.Size(112, 21)
        Me.txtNerve.TabIndex = 2
        '
        'lblNerve
        '
        Me.lblNerve.AutoSize = True
        Me.lblNerve.Location = New System.Drawing.Point(383, 33)
        Me.lblNerve.Name = "lblNerve"
        Me.lblNerve.Size = New System.Drawing.Size(39, 13)
        Me.lblNerve.TabIndex = 6
        Me.lblNerve.Text = "Nerve:"
        '
        'txtEnergy
        '
        Me.txtEnergy.BackColor = System.Drawing.Color.White
        Me.txtEnergy.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEnergy.ForeColor = System.Drawing.Color.Sienna
        Me.txtEnergy.Location = New System.Drawing.Point(254, 30)
        Me.txtEnergy.Name = "txtEnergy"
        Me.txtEnergy.ReadOnly = True
        Me.txtEnergy.Size = New System.Drawing.Size(112, 21)
        Me.txtEnergy.TabIndex = 1
        '
        'lblEnergy
        '
        Me.lblEnergy.AutoSize = True
        Me.lblEnergy.Location = New System.Drawing.Point(203, 33)
        Me.lblEnergy.Name = "lblEnergy"
        Me.lblEnergy.Size = New System.Drawing.Size(43, 13)
        Me.lblEnergy.TabIndex = 4
        Me.lblEnergy.Text = "Energy:"
        '
        'txtHitPoints
        '
        Me.txtHitPoints.BackColor = System.Drawing.Color.White
        Me.txtHitPoints.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHitPoints.ForeColor = System.Drawing.Color.Sienna
        Me.txtHitPoints.Location = New System.Drawing.Point(68, 30)
        Me.txtHitPoints.Name = "txtHitPoints"
        Me.txtHitPoints.ReadOnly = True
        Me.txtHitPoints.Size = New System.Drawing.Size(112, 21)
        Me.txtHitPoints.TabIndex = 0
        '
        'lblHitpoints
        '
        Me.lblHitpoints.AutoSize = True
        Me.lblHitpoints.Location = New System.Drawing.Point(17, 33)
        Me.lblHitpoints.Name = "lblHitpoints"
        Me.lblHitpoints.Size = New System.Drawing.Size(49, 13)
        Me.lblHitpoints.TabIndex = 2
        Me.lblHitpoints.Text = "HitPoints"
        '
        'grpMain
        '
        Me.grpMain.Controls.Add(Me.btnTargetTrain)
        Me.grpMain.Controls.Add(Me.chkSportsStudies)
        Me.grpMain.Controls.Add(Me.txtIQ)
        Me.grpMain.Controls.Add(Me.lblIQ)
        Me.grpMain.Controls.Add(Me.lblGTCBonus)
        Me.grpMain.Controls.Add(Me.cboGTC)
        Me.grpMain.Controls.Add(Me.lblTrainingCompound)
        Me.grpMain.Controls.Add(Me.lblValAwake)
        Me.grpMain.Controls.Add(Me.cboHouse)
        Me.grpMain.Controls.Add(Me.lblHouse)
        Me.grpMain.Controls.Add(Me.txtPoints)
        Me.grpMain.Controls.Add(Me.lblPoints)
        Me.grpMain.Controls.Add(Me.txtLevel)
        Me.grpMain.Controls.Add(Me.lblLevel)
        Me.grpMain.Location = New System.Drawing.Point(8, 1)
        Me.grpMain.Name = "grpMain"
        Me.grpMain.Size = New System.Drawing.Size(592, 141)
        Me.grpMain.TabIndex = 0
        Me.grpMain.TabStop = False
        Me.grpMain.Text = "Please Enter your Mobster Details here to get Training Predictions"
        Me.grpMain.UseCompatibleTextRendering = True
        '
        'btnTargetTrain
        '
        Me.btnTargetTrain.Location = New System.Drawing.Point(418, 107)
        Me.btnTargetTrain.Name = "btnTargetTrain"
        Me.btnTargetTrain.Size = New System.Drawing.Size(75, 23)
        Me.btnTargetTrain.TabIndex = 11
        Me.btnTargetTrain.Text = "Target Train"
        Me.btnTargetTrain.UseVisualStyleBackColor = True
        '
        'lblIQ
        '
        Me.lblIQ.AutoSize = True
        Me.lblIQ.Location = New System.Drawing.Point(17, 108)
        Me.lblIQ.Name = "lblIQ"
        Me.lblIQ.Size = New System.Drawing.Size(18, 13)
        Me.lblIQ.TabIndex = 10
        Me.lblIQ.Text = "IQ"
        '
        'lblGTCBonus
        '
        Me.lblGTCBonus.AutoSize = True
        Me.lblGTCBonus.ForeColor = System.Drawing.Color.Blue
        Me.lblGTCBonus.Location = New System.Drawing.Point(499, 68)
        Me.lblGTCBonus.Name = "lblGTCBonus"
        Me.lblGTCBonus.Size = New System.Drawing.Size(40, 13)
        Me.lblGTCBonus.TabIndex = 9
        Me.lblGTCBonus.Text = "Bonus:"
        '
        'lblTrainingCompound
        '
        Me.lblTrainingCompound.AutoSize = True
        Me.lblTrainingCompound.Location = New System.Drawing.Point(204, 68)
        Me.lblTrainingCompound.Name = "lblTrainingCompound"
        Me.lblTrainingCompound.Size = New System.Drawing.Size(102, 13)
        Me.lblTrainingCompound.TabIndex = 7
        Me.lblTrainingCompound.Text = "Training Compound:"
        '
        'lblValAwake
        '
        Me.lblValAwake.AutoSize = True
        Me.lblValAwake.ForeColor = System.Drawing.Color.Blue
        Me.lblValAwake.Location = New System.Drawing.Point(499, 31)
        Me.lblValAwake.Name = "lblValAwake"
        Me.lblValAwake.Size = New System.Drawing.Size(46, 13)
        Me.lblValAwake.TabIndex = 6
        Me.lblValAwake.Text = "Awake: "
        '
        'lblHouse
        '
        Me.lblHouse.AutoSize = True
        Me.lblHouse.Location = New System.Drawing.Point(265, 31)
        Me.lblHouse.Name = "lblHouse"
        Me.lblHouse.Size = New System.Drawing.Size(41, 13)
        Me.lblHouse.TabIndex = 4
        Me.lblHouse.Text = "House:"
        '
        'lblPoints
        '
        Me.lblPoints.AutoSize = True
        Me.lblPoints.Location = New System.Drawing.Point(17, 68)
        Me.lblPoints.Name = "lblPoints"
        Me.lblPoints.Size = New System.Drawing.Size(36, 13)
        Me.lblPoints.TabIndex = 2
        Me.lblPoints.Text = "Points"
        '
        'lblLevel
        '
        Me.lblLevel.AutoSize = True
        Me.lblLevel.Location = New System.Drawing.Point(17, 31)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(33, 13)
        Me.lblLevel.TabIndex = 0
        Me.lblLevel.Text = "Level"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox1)
        Me.TabPage2.Controls.Add(Me.grpPetDetails)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(5)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(606, 318)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Dog Trainer"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPetNerve)
        Me.GroupBox1.Controls.Add(Me.lblPetNerve)
        Me.GroupBox1.Controls.Add(Me.txtPetPointTrain)
        Me.GroupBox1.Controls.Add(Me.lblPointTrain)
        Me.GroupBox1.Controls.Add(Me.txtPetStatGain)
        Me.GroupBox1.Controls.Add(Me.lblStatGain)
        Me.GroupBox1.Controls.Add(Me.txtPetEnergy)
        Me.GroupBox1.Controls.Add(Me.lblPetEnergy)
        Me.GroupBox1.Controls.Add(Me.txtPetHP)
        Me.GroupBox1.Controls.Add(Me.lblPetHP)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 152)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(591, 120)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Training Predictions"
        '
        'txtPetNerve
        '
        Me.txtPetNerve.BackColor = System.Drawing.Color.White
        Me.txtPetNerve.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPetNerve.ForeColor = System.Drawing.Color.Sienna
        Me.txtPetNerve.Location = New System.Drawing.Point(469, 30)
        Me.txtPetNerve.Name = "txtPetNerve"
        Me.txtPetNerve.ReadOnly = True
        Me.txtPetNerve.Size = New System.Drawing.Size(112, 21)
        Me.txtPetNerve.TabIndex = 11
        '
        'lblPetNerve
        '
        Me.lblPetNerve.AutoSize = True
        Me.lblPetNerve.Location = New System.Drawing.Point(418, 33)
        Me.lblPetNerve.Name = "lblPetNerve"
        Me.lblPetNerve.Size = New System.Drawing.Size(39, 13)
        Me.lblPetNerve.TabIndex = 12
        Me.lblPetNerve.Text = "Nerve:"
        '
        'txtPetPointTrain
        '
        Me.txtPetPointTrain.BackColor = System.Drawing.Color.White
        Me.txtPetPointTrain.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPetPointTrain.ForeColor = System.Drawing.Color.OrangeRed
        Me.txtPetPointTrain.Location = New System.Drawing.Point(379, 76)
        Me.txtPetPointTrain.Name = "txtPetPointTrain"
        Me.txtPetPointTrain.ReadOnly = True
        Me.txtPetPointTrain.Size = New System.Drawing.Size(112, 23)
        Me.txtPetPointTrain.TabIndex = 3
        '
        'lblPointTrain
        '
        Me.lblPointTrain.AutoSize = True
        Me.lblPointTrain.Location = New System.Drawing.Point(303, 81)
        Me.lblPointTrain.Name = "lblPointTrain"
        Me.lblPointTrain.Size = New System.Drawing.Size(61, 13)
        Me.lblPointTrain.TabIndex = 10
        Me.lblPointTrain.Text = "Point Train:"
        '
        'txtPetStatGain
        '
        Me.txtPetStatGain.BackColor = System.Drawing.Color.White
        Me.txtPetStatGain.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPetStatGain.ForeColor = System.Drawing.Color.SeaGreen
        Me.txtPetStatGain.Location = New System.Drawing.Point(165, 76)
        Me.txtPetStatGain.Name = "txtPetStatGain"
        Me.txtPetStatGain.ReadOnly = True
        Me.txtPetStatGain.Size = New System.Drawing.Size(112, 23)
        Me.txtPetStatGain.TabIndex = 2
        '
        'lblStatGain
        '
        Me.lblStatGain.AutoSize = True
        Me.lblStatGain.Location = New System.Drawing.Point(91, 81)
        Me.lblStatGain.Name = "lblStatGain"
        Me.lblStatGain.Size = New System.Drawing.Size(54, 13)
        Me.lblStatGain.TabIndex = 8
        Me.lblStatGain.Text = "Stat Gain:"
        '
        'txtPetEnergy
        '
        Me.txtPetEnergy.BackColor = System.Drawing.Color.White
        Me.txtPetEnergy.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPetEnergy.ForeColor = System.Drawing.Color.Sienna
        Me.txtPetEnergy.Location = New System.Drawing.Point(274, 30)
        Me.txtPetEnergy.Name = "txtPetEnergy"
        Me.txtPetEnergy.ReadOnly = True
        Me.txtPetEnergy.Size = New System.Drawing.Size(112, 21)
        Me.txtPetEnergy.TabIndex = 1
        '
        'lblPetEnergy
        '
        Me.lblPetEnergy.AutoSize = True
        Me.lblPetEnergy.Location = New System.Drawing.Point(219, 33)
        Me.lblPetEnergy.Name = "lblPetEnergy"
        Me.lblPetEnergy.Size = New System.Drawing.Size(43, 13)
        Me.lblPetEnergy.TabIndex = 4
        Me.lblPetEnergy.Text = "Energy:"
        '
        'txtPetHP
        '
        Me.txtPetHP.BackColor = System.Drawing.Color.White
        Me.txtPetHP.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPetHP.ForeColor = System.Drawing.Color.Sienna
        Me.txtPetHP.Location = New System.Drawing.Point(80, 30)
        Me.txtPetHP.Name = "txtPetHP"
        Me.txtPetHP.ReadOnly = True
        Me.txtPetHP.Size = New System.Drawing.Size(112, 21)
        Me.txtPetHP.TabIndex = 0
        '
        'lblPetHP
        '
        Me.lblPetHP.AutoSize = True
        Me.lblPetHP.Location = New System.Drawing.Point(8, 33)
        Me.lblPetHP.Name = "lblPetHP"
        Me.lblPetHP.Size = New System.Drawing.Size(52, 13)
        Me.lblPetHP.TabIndex = 2
        Me.lblPetHP.Text = "HitPoints:"
        '
        'grpPetDetails
        '
        Me.grpPetDetails.Controls.Add(Me.btnPetTargetTrain)
        Me.grpPetDetails.Controls.Add(Me.txtPetPoints)
        Me.grpPetDetails.Controls.Add(Me.lblPetPoints)
        Me.grpPetDetails.Controls.Add(Me.chkPetSchool)
        Me.grpPetDetails.Controls.Add(Me.lblKennelAwake)
        Me.grpPetDetails.Controls.Add(Me.txtPetLevel)
        Me.grpPetDetails.Controls.Add(Me.lblPetLevel)
        Me.grpPetDetails.Controls.Add(Me.cboKennel)
        Me.grpPetDetails.Controls.Add(Me.lblKennel)
        Me.grpPetDetails.Location = New System.Drawing.Point(8, 16)
        Me.grpPetDetails.Name = "grpPetDetails"
        Me.grpPetDetails.Size = New System.Drawing.Size(590, 108)
        Me.grpPetDetails.TabIndex = 0
        Me.grpPetDetails.TabStop = False
        Me.grpPetDetails.Text = "Please Enter your Pet Details to get Training Predictions"
        '
        'lblPetPoints
        '
        Me.lblPetPoints.AutoSize = True
        Me.lblPetPoints.Location = New System.Drawing.Point(17, 74)
        Me.lblPetPoints.Name = "lblPetPoints"
        Me.lblPetPoints.Size = New System.Drawing.Size(36, 13)
        Me.lblPetPoints.TabIndex = 16
        Me.lblPetPoints.Text = "Points"
        '
        'lblKennelAwake
        '
        Me.lblKennelAwake.AutoSize = True
        Me.lblKennelAwake.ForeColor = System.Drawing.Color.Blue
        Me.lblKennelAwake.Location = New System.Drawing.Point(485, 37)
        Me.lblKennelAwake.Name = "lblKennelAwake"
        Me.lblKennelAwake.Size = New System.Drawing.Size(46, 13)
        Me.lblKennelAwake.TabIndex = 9
        Me.lblKennelAwake.Text = "Awake: "
        '
        'lblPetLevel
        '
        Me.lblPetLevel.AutoSize = True
        Me.lblPetLevel.Location = New System.Drawing.Point(17, 37)
        Me.lblPetLevel.Name = "lblPetLevel"
        Me.lblPetLevel.Size = New System.Drawing.Size(33, 13)
        Me.lblPetLevel.TabIndex = 13
        Me.lblPetLevel.Text = "Level"
        '
        'lblKennel
        '
        Me.lblKennel.AutoSize = True
        Me.lblKennel.Location = New System.Drawing.Point(251, 37)
        Me.lblKennel.Name = "lblKennel"
        Me.lblKennel.Size = New System.Drawing.Size(43, 13)
        Me.lblKennel.TabIndex = 7
        Me.lblKennel.Text = "Kennel:"
        '
        'btnPetTargetTrain
        '
        Me.btnPetTargetTrain.Location = New System.Drawing.Point(456, 69)
        Me.btnPetTargetTrain.Name = "btnPetTargetTrain"
        Me.btnPetTargetTrain.Size = New System.Drawing.Size(75, 23)
        Me.btnPetTargetTrain.TabIndex = 17
        Me.btnPetTargetTrain.Text = "Target Train"
        Me.btnPetTargetTrain.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 344)
        Me.Controls.Add(Me.tbMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TMU Trainer - by NicotineOG[1429]"
        Me.tbMain.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.grpTraining.ResumeLayout(False)
        Me.grpTraining.PerformLayout()
        Me.grpMain.ResumeLayout(False)
        Me.grpMain.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpPetDetails.ResumeLayout(False)
        Me.grpPetDetails.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ttpDefault As System.Windows.Forms.ToolTip
    Friend WithEvents tbMain As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grpTraining As System.Windows.Forms.GroupBox
    Friend WithEvents txtPointTrain As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSingleTrain As System.Windows.Forms.TextBox
    Friend WithEvents lblStatsGained As System.Windows.Forms.Label
    Friend WithEvents txtNerve As System.Windows.Forms.TextBox
    Friend WithEvents lblNerve As System.Windows.Forms.Label
    Friend WithEvents txtEnergy As System.Windows.Forms.TextBox
    Friend WithEvents lblEnergy As System.Windows.Forms.Label
    Friend WithEvents txtHitPoints As System.Windows.Forms.TextBox
    Friend WithEvents lblHitpoints As System.Windows.Forms.Label
    Friend WithEvents grpMain As System.Windows.Forms.GroupBox
    Friend WithEvents chkSportsStudies As System.Windows.Forms.CheckBox
    Friend WithEvents txtIQ As System.Windows.Forms.TextBox
    Friend WithEvents lblIQ As System.Windows.Forms.Label
    Friend WithEvents lblGTCBonus As System.Windows.Forms.Label
    Friend WithEvents cboGTC As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrainingCompound As System.Windows.Forms.Label
    Friend WithEvents lblValAwake As System.Windows.Forms.Label
    Friend WithEvents cboHouse As System.Windows.Forms.ComboBox
    Friend WithEvents lblHouse As System.Windows.Forms.Label
    Friend WithEvents txtPoints As System.Windows.Forms.TextBox
    Friend WithEvents lblPoints As System.Windows.Forms.Label
    Friend WithEvents txtLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents grpPetDetails As System.Windows.Forms.GroupBox
    Friend WithEvents lblKennelAwake As System.Windows.Forms.Label
    Friend WithEvents cboKennel As System.Windows.Forms.ComboBox
    Friend WithEvents lblKennel As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPetPointTrain As System.Windows.Forms.TextBox
    Friend WithEvents lblPointTrain As System.Windows.Forms.Label
    Friend WithEvents txtPetStatGain As System.Windows.Forms.TextBox
    Friend WithEvents lblStatGain As System.Windows.Forms.Label
    Friend WithEvents txtPetEnergy As System.Windows.Forms.TextBox
    Friend WithEvents lblPetEnergy As System.Windows.Forms.Label
    Friend WithEvents txtPetHP As System.Windows.Forms.TextBox
    Friend WithEvents lblPetHP As System.Windows.Forms.Label
    Friend WithEvents txtPetPoints As System.Windows.Forms.TextBox
    Friend WithEvents lblPetPoints As System.Windows.Forms.Label
    Friend WithEvents chkPetSchool As System.Windows.Forms.CheckBox
    Friend WithEvents txtPetLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblPetLevel As System.Windows.Forms.Label
    Friend WithEvents txtPetNerve As System.Windows.Forms.TextBox
    Friend WithEvents lblPetNerve As System.Windows.Forms.Label
    Friend WithEvents btnTargetTrain As System.Windows.Forms.Button
    Friend WithEvents btnPetTargetTrain As System.Windows.Forms.Button

End Class

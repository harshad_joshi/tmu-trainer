﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTargetTrain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTargetTrain))
        Me.txtTargetStats = New System.Windows.Forms.TextBox()
        Me.lblTargetStats = New System.Windows.Forms.Label()
        Me.grpDetails = New System.Windows.Forms.GroupBox()
        Me.txtStatGain = New System.Windows.Forms.TextBox()
        Me.lblStatGain = New System.Windows.Forms.Label()
        Me.txtSportsStudies = New System.Windows.Forms.TextBox()
        Me.lblSportsStudies = New System.Windows.Forms.Label()
        Me.txtIQ = New System.Windows.Forms.TextBox()
        Me.lblIQ = New System.Windows.Forms.Label()
        Me.txtGTC = New System.Windows.Forms.TextBox()
        Me.lblGangCompound = New System.Windows.Forms.Label()
        Me.txtAwake = New System.Windows.Forms.TextBox()
        Me.lblAwake = New System.Windows.Forms.Label()
        Me.txtLevel = New System.Windows.Forms.TextBox()
        Me.lblLevel = New System.Windows.Forms.Label()
        Me.txtPointsRequired = New System.Windows.Forms.TextBox()
        Me.lblPointsRequired = New System.Windows.Forms.Label()
        Me.btnTargetPoints = New System.Windows.Forms.Button()
        Me.grpDetails.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTargetStats
        '
        Me.txtTargetStats.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTargetStats.Location = New System.Drawing.Point(112, 355)
        Me.txtTargetStats.Name = "txtTargetStats"
        Me.txtTargetStats.Size = New System.Drawing.Size(136, 23)
        Me.txtTargetStats.TabIndex = 15
        '
        'lblTargetStats
        '
        Me.lblTargetStats.AutoSize = True
        Me.lblTargetStats.Location = New System.Drawing.Point(22, 358)
        Me.lblTargetStats.Name = "lblTargetStats"
        Me.lblTargetStats.Size = New System.Drawing.Size(68, 13)
        Me.lblTargetStats.TabIndex = 14
        Me.lblTargetStats.Text = "Target Stats:"
        '
        'grpDetails
        '
        Me.grpDetails.Controls.Add(Me.txtStatGain)
        Me.grpDetails.Controls.Add(Me.lblStatGain)
        Me.grpDetails.Controls.Add(Me.txtSportsStudies)
        Me.grpDetails.Controls.Add(Me.lblSportsStudies)
        Me.grpDetails.Controls.Add(Me.txtIQ)
        Me.grpDetails.Controls.Add(Me.lblIQ)
        Me.grpDetails.Controls.Add(Me.txtGTC)
        Me.grpDetails.Controls.Add(Me.lblGangCompound)
        Me.grpDetails.Controls.Add(Me.txtAwake)
        Me.grpDetails.Controls.Add(Me.lblAwake)
        Me.grpDetails.Controls.Add(Me.txtLevel)
        Me.grpDetails.Controls.Add(Me.lblLevel)
        Me.grpDetails.Location = New System.Drawing.Point(6, 4)
        Me.grpDetails.Name = "grpDetails"
        Me.grpDetails.Size = New System.Drawing.Size(249, 329)
        Me.grpDetails.TabIndex = 16
        Me.grpDetails.TabStop = False
        Me.grpDetails.Text = "Details"
        '
        'txtStatGain
        '
        Me.txtStatGain.BackColor = System.Drawing.SystemColors.Window
        Me.txtStatGain.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStatGain.ForeColor = System.Drawing.Color.Maroon
        Me.txtStatGain.Location = New System.Drawing.Point(106, 262)
        Me.txtStatGain.Name = "txtStatGain"
        Me.txtStatGain.ReadOnly = True
        Me.txtStatGain.Size = New System.Drawing.Size(136, 23)
        Me.txtStatGain.TabIndex = 23
        '
        'lblStatGain
        '
        Me.lblStatGain.AutoSize = True
        Me.lblStatGain.Location = New System.Drawing.Point(16, 265)
        Me.lblStatGain.Name = "lblStatGain"
        Me.lblStatGain.Size = New System.Drawing.Size(54, 13)
        Me.lblStatGain.TabIndex = 22
        Me.lblStatGain.Text = "Stat Gain:"
        '
        'txtSportsStudies
        '
        Me.txtSportsStudies.BackColor = System.Drawing.SystemColors.Window
        Me.txtSportsStudies.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSportsStudies.ForeColor = System.Drawing.Color.Maroon
        Me.txtSportsStudies.Location = New System.Drawing.Point(106, 213)
        Me.txtSportsStudies.Name = "txtSportsStudies"
        Me.txtSportsStudies.ReadOnly = True
        Me.txtSportsStudies.Size = New System.Drawing.Size(136, 23)
        Me.txtSportsStudies.TabIndex = 21
        '
        'lblSportsStudies
        '
        Me.lblSportsStudies.AutoSize = True
        Me.lblSportsStudies.Location = New System.Drawing.Point(16, 216)
        Me.lblSportsStudies.Name = "lblSportsStudies"
        Me.lblSportsStudies.Size = New System.Drawing.Size(78, 13)
        Me.lblSportsStudies.TabIndex = 20
        Me.lblSportsStudies.Text = "Sports Studies:"
        '
        'txtIQ
        '
        Me.txtIQ.BackColor = System.Drawing.SystemColors.Window
        Me.txtIQ.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIQ.ForeColor = System.Drawing.Color.Maroon
        Me.txtIQ.Location = New System.Drawing.Point(106, 166)
        Me.txtIQ.Name = "txtIQ"
        Me.txtIQ.ReadOnly = True
        Me.txtIQ.Size = New System.Drawing.Size(136, 23)
        Me.txtIQ.TabIndex = 19
        '
        'lblIQ
        '
        Me.lblIQ.AutoSize = True
        Me.lblIQ.Location = New System.Drawing.Point(16, 169)
        Me.lblIQ.Name = "lblIQ"
        Me.lblIQ.Size = New System.Drawing.Size(21, 13)
        Me.lblIQ.TabIndex = 18
        Me.lblIQ.Text = "IQ:"
        '
        'txtGTC
        '
        Me.txtGTC.BackColor = System.Drawing.SystemColors.Window
        Me.txtGTC.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtGTC.ForeColor = System.Drawing.Color.Maroon
        Me.txtGTC.Location = New System.Drawing.Point(106, 124)
        Me.txtGTC.Name = "txtGTC"
        Me.txtGTC.ReadOnly = True
        Me.txtGTC.Size = New System.Drawing.Size(136, 23)
        Me.txtGTC.TabIndex = 17
        '
        'lblGangCompound
        '
        Me.lblGangCompound.AutoSize = True
        Me.lblGangCompound.Location = New System.Drawing.Point(16, 127)
        Me.lblGangCompound.Name = "lblGangCompound"
        Me.lblGangCompound.Size = New System.Drawing.Size(90, 13)
        Me.lblGangCompound.TabIndex = 16
        Me.lblGangCompound.Text = "Gang Compound:"
        '
        'txtAwake
        '
        Me.txtAwake.BackColor = System.Drawing.SystemColors.Window
        Me.txtAwake.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAwake.ForeColor = System.Drawing.Color.Maroon
        Me.txtAwake.Location = New System.Drawing.Point(106, 85)
        Me.txtAwake.Name = "txtAwake"
        Me.txtAwake.ReadOnly = True
        Me.txtAwake.Size = New System.Drawing.Size(136, 23)
        Me.txtAwake.TabIndex = 15
        '
        'lblAwake
        '
        Me.lblAwake.AutoSize = True
        Me.lblAwake.Location = New System.Drawing.Point(16, 88)
        Me.lblAwake.Name = "lblAwake"
        Me.lblAwake.Size = New System.Drawing.Size(43, 13)
        Me.lblAwake.TabIndex = 14
        Me.lblAwake.Text = "Awake:"
        '
        'txtLevel
        '
        Me.txtLevel.BackColor = System.Drawing.SystemColors.Window
        Me.txtLevel.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLevel.ForeColor = System.Drawing.Color.Maroon
        Me.txtLevel.Location = New System.Drawing.Point(106, 47)
        Me.txtLevel.Name = "txtLevel"
        Me.txtLevel.ReadOnly = True
        Me.txtLevel.Size = New System.Drawing.Size(136, 23)
        Me.txtLevel.TabIndex = 13
        '
        'lblLevel
        '
        Me.lblLevel.AutoSize = True
        Me.lblLevel.Location = New System.Drawing.Point(16, 50)
        Me.lblLevel.Name = "lblLevel"
        Me.lblLevel.Size = New System.Drawing.Size(36, 13)
        Me.lblLevel.TabIndex = 12
        Me.lblLevel.Text = "Level:"
        '
        'txtPointsRequired
        '
        Me.txtPointsRequired.BackColor = System.Drawing.SystemColors.Window
        Me.txtPointsRequired.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPointsRequired.ForeColor = System.Drawing.Color.SeaGreen
        Me.txtPointsRequired.Location = New System.Drawing.Point(112, 448)
        Me.txtPointsRequired.Name = "txtPointsRequired"
        Me.txtPointsRequired.ReadOnly = True
        Me.txtPointsRequired.Size = New System.Drawing.Size(136, 23)
        Me.txtPointsRequired.TabIndex = 18
        '
        'lblPointsRequired
        '
        Me.lblPointsRequired.AutoSize = True
        Me.lblPointsRequired.Location = New System.Drawing.Point(22, 451)
        Me.lblPointsRequired.Name = "lblPointsRequired"
        Me.lblPointsRequired.Size = New System.Drawing.Size(85, 13)
        Me.lblPointsRequired.TabIndex = 17
        Me.lblPointsRequired.Text = "Points Required:"
        '
        'btnTargetPoints
        '
        Me.btnTargetPoints.Location = New System.Drawing.Point(67, 392)
        Me.btnTargetPoints.Name = "btnTargetPoints"
        Me.btnTargetPoints.Size = New System.Drawing.Size(140, 30)
        Me.btnTargetPoints.TabIndex = 19
        Me.btnTargetPoints.Text = "Tell me!"
        Me.btnTargetPoints.UseVisualStyleBackColor = True
        '
        'frmTargetTrain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(260, 502)
        Me.Controls.Add(Me.btnTargetPoints)
        Me.Controls.Add(Me.txtPointsRequired)
        Me.Controls.Add(Me.lblPointsRequired)
        Me.Controls.Add(Me.grpDetails)
        Me.Controls.Add(Me.txtTargetStats)
        Me.Controls.Add(Me.lblTargetStats)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTargetTrain"
        Me.ShowInTaskbar = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Target Train"
        Me.grpDetails.ResumeLayout(False)
        Me.grpDetails.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTargetStats As System.Windows.Forms.TextBox
    Friend WithEvents lblTargetStats As System.Windows.Forms.Label
    Friend WithEvents grpDetails As System.Windows.Forms.GroupBox
    Friend WithEvents txtStatGain As System.Windows.Forms.TextBox
    Friend WithEvents lblStatGain As System.Windows.Forms.Label
    Friend WithEvents txtSportsStudies As System.Windows.Forms.TextBox
    Friend WithEvents lblSportsStudies As System.Windows.Forms.Label
    Friend WithEvents txtIQ As System.Windows.Forms.TextBox
    Friend WithEvents lblIQ As System.Windows.Forms.Label
    Friend WithEvents txtGTC As System.Windows.Forms.TextBox
    Friend WithEvents lblGangCompound As System.Windows.Forms.Label
    Friend WithEvents txtAwake As System.Windows.Forms.TextBox
    Friend WithEvents lblAwake As System.Windows.Forms.Label
    Friend WithEvents txtLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblLevel As System.Windows.Forms.Label
    Friend WithEvents txtPointsRequired As System.Windows.Forms.TextBox
    Friend WithEvents lblPointsRequired As System.Windows.Forms.Label
    Friend WithEvents btnTargetPoints As System.Windows.Forms.Button
End Class
